import json
from os import listdir, path
import re
from collections import Counter, defaultdict
import csv
import random


def find_files(directory):
    """ This method takes a directory as argument, we can iterate over directory items (files to be processed),
    returns files list"""
    files = []
    for filename in listdir(directory):
        if filename.endswith(".out"):
            files.append(directory + "/" + filename)
    return files


def extract_tweets(infile):
    """takes file, reads file, returns list of tuples with text of tweet and username"""
    tweets = []
    for line in open(infile, 'r'):
        if line.startswith('{'):
            if 'text' in line and 'retweeted_status' not in line:
                tweets.append((json.loads(line)['text'], json.loads(line)['user']['screen_name']))
    return tweets


def remove_xrated_data(lst):
    """takes list of tweet user tuples, returns list of tweet user tuples without terribly annoying english x-rated data"""
    xrated = ['nude', 'sex', 'girl', 'xxx', 'porn', 'teen']
    filtered = []
    for tweet, user in lst:
        remove = False
        for word in xrated:
            if word in tweet.lower():
                remove = True
        if not remove:
            filtered.append((tweet, user))

    return filtered


def remove_urls(lst):
    """Takes list of tuples as argument and returns a list of tuples in which the tweets
    don't contain any urls anymore"""
    new_lst = []
    for tweet, user in lst:
        tweet = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', tweet, flags=re.MULTILINE)
        new_lst.append((tweet, user))

    return new_lst


def remove_accidental_tweets(tweet_list):
    """remove tweets in which words occur containing party names but
    with no direct party meaning e.g. 'rasp'"""
    ambiguous_parties = ["sp", "gl", "cda", "cu", "50+"]
    other_parties = ["christen-democratisch appèl", "christenunie",
                     "d66", "democraten 66", "groenlinks", "pvda", "partij van de arbeid",
                     "pvdd", "partij voor de dieren", "partijvddieren", "pvv", "partij voor de vrijheid",
                     "sgp", "staatkundig gereformeerde partij", "socialistische partij",
                     "vvd", "volkspartij voor vrijheid en democratie", "50plus"]
    new_lst = []
    for tweet, user in tweet_list:
        tweet = tweet.lower()
        user = user.lower()
        tweet_split = tweet.lower().split()
        not_appended = True
        while not_appended:
            for word in tweet_split:
                for item in ambiguous_parties:
                    if item in word and len(word) == len(item):
                        new_lst.append((tweet, user))
                        not_appended = False
                        break
                    elif item in word and word.startswith("@") and not word.endswith("afspeellijst:"):
                        if "-" in word or "vandaag" in word:
                            new_lst.append((tweet, user))
                            not_appended = False
                            break
                        elif "nl" in word and len(word) == 5:
                            new_lst.append((tweet, user))
                            not_appended = False
                            break
                        elif "_" in word and len(word) == 5:
                            new_lst.append((tweet, user))
                            not_appended = False
                            break
                    elif item in word and word.startswith("#"):
                        new_lst.append((tweet, user))
                        not_appended = False
                        break
                    elif item in word and "-" in word and not word.endswith("-"):
                        new_lst.append((tweet, user))
                        not_appended = False
                for party in other_parties:
                    if party in word:
                        new_lst.append((tweet, user))
                        not_appended = False
                        break
            not_appended = False
    return list(set(new_lst))

parties = ["cda", "christen-democratisch appèl", "cu", "christenunie",
           "d66", "democraten 66", "gl", "groenlinks", "pvda", "partij van de arbeid",
           "pvdd", "partij voor de dieren", "partijvddieren", "pvv", "partij voor de vrijheid",
           "sgp", "staatkundig gereformeerde partij", "sp", "socialistische partij",
           "vvd", "volkspartij voor vrijheid en democratie", "50plus",
           "50+"]


def remove_tweets_with_multiple_parties(lst, parties):
    """takes lst of tuples and list of parties, returns a list of tweet user tuples
    without any tweets mentioning multiple different parties"""
    filtered = []
    correct = [('cda', 'christen-democratisch appèl'), ('cu', 'christenunie'), ('d66', 'democraten 66'), ('gl', 'groenlinks'),
               ('pvda', 'partij van de arbeid'), ('pvdd', 'partij voor de dieren'), ('partij voor de dieren', 'partijvddieren'), ('pvv', 'partij voor de vrijheid'),
               ('sgp', 'staatkundig gereformeerde partij'), ('sp', 'socialistische partij'), ('vvd', 'volskpartij voor vrijheid en democratie'),
               ('50plus', '50+')]
    for tweet, user in lst:
        numbers = ()
        for party in parties:
            if party in tweet.lower():
                numbers = numbers + (party,)
        if len(numbers) < 3:
            if len(numbers) == 1:
                filtered.append((tweet, user))
            elif len(numbers) == 2 and numbers in correct:
                filtered.append((tweet, user))

    return filtered


def remove_tweets_from_parties(lst):
    """expects a lowercased list of tweet, user tuples, returns only those tuples
    that are not from parties themselves"""
    party_accounts = ["d66", "pvda", "spnl", "pvdd", "vvd", "cdavandaag",
                      "_pvv", "groenlinks", "christenunie", "50plusfractie", "sgpnieuws"]
    blacklisted = []
    for index, tup in enumerate(lst):
        tweet, user = tup
        for party in party_accounts:
            if user == party:
                blacklisted.append(index)
                break
    if blacklisted:
        for index in sorted(blacklisted, reverse=True):
            lst.remove(lst[index])

    return lst


def filter_users_with_mult_tweets(tweet_list, party_list):
    """input is a list with tweet,user tuples, and a list of parties
    a filtered list is returned in which all users only have one tweet per party
    (so the second tweet about party X from user Y would be removed"""
    userdict = defaultdict(list)
    for tweet, user in tweet_list:
        userdict[user].append(tweet)
    for user, lst in userdict.items():
        if len(lst) > 1:
            parties_mentioned = defaultdict(set)
            for index, item in enumerate(lst):
                for party in party_list:
                    if party in item:
                        parties_mentioned[index].add(party)
            parties_secure = []
            blacklisted = []
            for index, parties in parties_mentioned.items():
                for party in parties:
                    if party in parties_secure:
                        blacklisted.append(index)
                        break
                    elif party not in parties_secure:
                        parties_secure.append(party)
            if blacklisted:
                for item in sorted(blacklisted, reverse=True):
                    tweet_list.remove((lst[item], user))

    return tweet_list


def select_hashtags(tweets):
    """selects tweets with hashtags like '#stempvv' and puts them in training folder
    input is a list of tweets as strings, users have already been removed!
    used for distant supervision training set"""
    pos = []
    neg = []
    abbvs = ["cda", "cu", "d66", "gl", "pvda", "pvdd", "pvv", "sgp", "sp", "vvd", "50plus"]
    pos_hash = ["#ikstem", "#stem"]
    neg_hash = ["#ikstemgeen", "#stemgeen", "#stemniet", "#ikstemniet"]
    script_dir = path.dirname(__file__)
    for tweet in tweets:
        for word in tweet.split():
            for item in pos_hash:
                for abv in abbvs:
                    if word.startswith(item) and abv in word and "niet" not in word and "geen" not in word:
                        pos.append(tweet)
                        break
            for item in neg_hash:
                for abv in abbvs:
                    if word.startswith(item) and abv in word:
                        neg.append(tweet)
                        break
    double = set(pos).intersection(set(neg))
    for item in double:
        pos.remove(item)
        neg.remove(item)
    for item in set(pos):
        rel_path = "classifier/training/pos/positive_tweets.csv"
        abs_file_path = path.join(script_dir, rel_path)
        with open(abs_file_path, "a", newline="") as f:
            writer = csv.writer(f, delimiter=" ")
            writer.writerow([item])
        tweets.remove(item)
    for item in set(neg):
        rel_path = "classifier/training/neg/negative_tweets.csv"
        abs_file_path = path.join(script_dir, rel_path)
        with open(abs_file_path, "a", newline="") as f:
            writer = csv.writer(f, delimiter=" ")
            writer.writerow([item])
        tweets.remove(item)
    return tweets


def select_smileys(tweets):
    """selects tweets with smileys and puts them in the training folders
    input is a list of tweets as strings, users have already been removed!
    used for distant supervision training set"""
    pos = []
    neg = []
    smiley = ":)"
    sadface = ":("
    script_dir = path.dirname(__file__)
    for tweet in tweets:
        if smiley in tweet and sadface in tweet:
            break
        elif smiley in tweet:
            pos.append(tweet)
        elif sadface in tweet:
            neg.append(tweet)
    for item in pos:
        rel_path = "classifier/training/pos/positive_tweets.csv"
        abs_file_path = path.join(script_dir, rel_path)
        with open(abs_file_path, "a", newline="") as f:
            writer = csv.writer(f, delimiter=" ")
            writer.writerow([item])
        tweets.remove(item)
    for item in neg:
        rel_path = "classifier/training/neg/negative_tweets.csv"
        abs_file_path = path.join(script_dir, rel_path)
        with open(abs_file_path, "a", newline="") as f:
            writer = csv.writer(f, delimiter=" ")
            writer.writerow([item])
        tweets.remove(item)
    return tweets


def write_to_file(tweets):
    """after processing, the political tweets are written to a csv"""
    script_dir = path.dirname(__file__)
    rel_path = "classifier/research_tweets.csv"
    abs_file_path = path.join(script_dir, rel_path)
    random.shuffle(tweets)
    for item in tweets:
        with open(abs_file_path, "a", newline="") as f:
            writer = csv.writer(f)
            writer.writerow([item])

    return tweets


def annotator():
    """open file specified below, read into a list, then shuffle, then annotate until you want to stop
    after which the remaining tweets are written to a new file (always change this filename)
    during annotation the labeled tweets are sent to positive and negative csvs"""
    tweets = []
    with open("INPUT NAME OF FILE THAT REMAINDER WAS PREVIOUSLY SENT TO, e.g. remainder.csv", "r") as f:
        reader = csv.reader(f)
        for row in reader:
            tweets.append(row)
    not_bored = True
    random.shuffle(tweets)
    while not_bored:
        for tweet in tweets:
            print("Please classify the following: ")
            print()
            print(tweet)
            print()
            action = input("CLASSIFY: p for positive, n for negative, enter for skip, stop for stop:  ")
            if action == "p":
                with open("classifier/testing/pos/positive_tweets.csv", "a", newline="") as file:
                    writer = csv.writer(file)
                    writer.writerow(tweet)
                tweets.remove(tweet)
            elif action == "n":
                with open("classifier/testing/neg/negative_tweets.csv", "a", newline="") as file:
                    writer = csv.writer(file)
                    writer.writerow(tweet)
                tweets.remove(tweet)
            elif action == "stop":
                not_bored = False
                break
            elif action == "":
                pass
    new_path = "ENTER FILE NAME TO SEND REMAINING DATA TO, e.g. remainder2.csv"
    script_dir = path.dirname(__file__)
    abs_file_path = path.join(script_dir, new_path)
    for tweet in tweets:
        with open(abs_file_path, "a", newline="") as remainder:
            writer = csv.writer(remainder)
            writer.writerow(tweet)

    return None


def get_training_data(path):
    """go through all .out files in specified directory, then process the data,
    remove lists within lists, then call select_smileys and select_hashtags to
    send the desired tweets to the training file"""
    for file in find_files(path):
        tweettuples = remove_urls(remove_xrated_data(extract_tweets(file)))
        tweets = [tweet for tweet, user in tweettuples]
        select_smileys(select_hashtags(tweets))

    return None


# get_training_data("training_data")

# do this once in order to get the processed test set, collapse the lists and write to test file
# research_data = [filter_users_with_mult_tweets(remove_tweets_from_parties(remove_tweets_with_multiple_parties(remove_accidental_tweets(remove_urls(remove_xrated_data(extract_tweets(file)))), parties)), parties) for file in find_files("tweets")]
# research_tweet_list = [tweet for lst in research_data for tweet, user in lst]
# research_tweets_to_file = write_to_file(research_tweet_list)
