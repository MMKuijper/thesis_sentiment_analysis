from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import time
import json

consumer_key = 'XXX'
consumer_secret = 'XXX'

access_token = 'XXX'
access_token_secret = 'XXX'


class Listener(StreamListener):

    def on_data(self, data):
        """append data to json file for that day"""
        try:
            print(data)
            saveFile = open('raw_tweets_1503.json', 'a')
            saveFile.write(data)
            saveFile.write('\n')
            saveFile.close()
            return True
        except BaseException as e:
            print('failed on data, ', str(e))
            time.sleep(55)

    def on_error(self, status):
        """error handling"""
        print(status)


auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

twitterStream = Stream(auth, Listener())

twitterStream.filter(track=["cda", "CDA", "Christen-Democratisch Appèl", "cu",
                            "CU", "ChristenUnie", "Christenunie", "d66", "D66",
                            "Democraten 66", "gl", "GL", "GroenLinks", "pvda",
                            "PVDA", "Partij van de Arbeid", "pvdd", "PVDD",
                            "PvdD", "Partij voor de Dieren", "pvv", "PVV",
                            "Partij voor de Vrijheid", "sgp", "SGP",
                            "Staatkundig Gereformeerde Partij", "sp", "SP",
                            "Socialistische Partij", "vvd", "VVD",
                            "Volkspartij voor Vrijheid en Democratie",
                            "50plus", "50Plus", "50PLUS", "50+"],
                     languages=["nl"])
