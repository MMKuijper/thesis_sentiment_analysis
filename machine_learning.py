import csv
import re
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn import metrics
from stop_words import get_stop_words
import numpy as np
import pandas as pd
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.pipeline import make_pipeline, Pipeline
from nltk.tokenize import TweetTokenizer
from sklearn import svm, linear_model
from scipy.sparse import csr_matrix
import nltk
from sklearn.naive_bayes import MultinomialNB, GaussianNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
from os import path

df = pd.read_csv("classifier/training/all_training.csv", header=0)
df1 = pd.read_csv("classifier/testing/all_testing_dev.csv", header=0)
df2 = pd.read_csv("classifier/testing/all_testing_real.csv", header=0)
# df3 = pd.read_csv("classifier/blinddata.csv", header=None)

df = df.append(df1)

text_train, y_train = df.tweet, df.label
text_test, y_test = df2.tweet, df2.label

# didn't use full list of stopwords, some were actually quite helpful in finding sentiment
stopwords = ['aan', 'al', 'als', 'andere', 'ben', 'bij', 'daar', 'dan', 'dat', 'de',
             'der', 'deze', 'die', 'dit', 'doch', 'doen', 'dus', 'een', 'en', 'er', 'ge', 'geweest',
             'haar', 'had', 'heb', 'hebben', 'heeft', 'hem', 'het', 'hier', 'hij', 'hoe', 'hun', 'iemand',
             'iets', 'ik', 'in', 'is', 'je', 'kan', 'kon', 'kunnen', 'maar', 'me', 'meer', 'men', 'met', 'mij',
             'mijn', 'moet', 'na', 'naar', 'nog', 'nu', 'of', 'om', 'omdat', 'onder', 'ons', 'over', 'reeds',
             'te', 'toch', 'toen', 'tot', 'u', 'uit', 'uw', 'van', 'want', 'waren', 'was', 'wat',
             'werd', 'wezen', 'wie', 'wil', 'zal', 'ze', 'zelf', 'zich', 'zij', 'zijn', 'zo', 'zonder', 'zou']


def identity(x):
    return x


def add_booster_word(tokenized_tweet):
    """adds booster words to tokenized tweet if it
    contains a certain hashtag construction,
    returns the tweet with extra booster words """
    tweet = tokenized_tweet
    pos = []
    neg = []
    abbvs = ["cda", "cu", "d66", "gl", "pvda", "pvdd", "pvv", "sgp", "sp", "vvd", "50plus"]
    pos_hash = ["#ikstem", "#stem"]
    neg_hash = ["#ikstemgeen", "#stemgeen", "#stemniet", "#ikstemniet"]
    for word in tweet:
        for item in pos_hash:
            for abv in abbvs:
                if word.lower().startswith(item) and abv in word.lower() and "niet" not in word.lower() and "geen" not in word.lower():
                    tokenized_tweet.append("mooi")
                    tokenized_tweet.append("leuk")

        for item in neg_hash:
            for abv in abbvs:
                if word.lower().startswith(item) and abv in word.lower():
                    tokenized_tweet.append("slecht")
                    tokenized_tweet.append("huilen")

    return tokenized_tweet


def remove_smileys(tokenized_tweet):
    """removes smileys from tokenized tweet, return tweet without smileys"""
    for word in tokenized_tweet:
        if word == ":)" or word == ":(":
            tokenized_tweet.remove(word)

    return tokenized_tweet


def tokenizer(tweet):
    """tokenizes tweet, returns tokenized tweet"""
    tknzr = TweetTokenizer(reduce_len=True)
    return tknzr.tokenize(tweet)


x_train = []
# x_train_with_smileys = []
for line in text_train:
    # x_train_with_smileys.append(tokenizer(line))
    x_train.append(remove_smileys(tokenizer(line)))


x_test = []
# x_test_with_smileys = []
for line in text_test:
    # x_train_with_smileys.append(tokenizer(line))
    x_test.append(add_booster_word(remove_smileys(tokenizer(line))))


vec_nb_1 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords, max_features=7000)
vec_nb_12 = TfidfVectorizer(preprocessor=identity, binary=True, tokenizer=identity, ngram_range=(1, 2), stop_words=stopwords, max_features=7000)
vec_nb_123 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, min_df=2, tokenizer=identity, ngram_range=(1, 3), stop_words=stopwords, max_features=8300)


vec_1 = TfidfVectorizer(preprocessor=identity, binary=True, min_df=10, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords)
vec_12 = TfidfVectorizer(preprocessor=identity, binary=True, min_df=10, tokenizer=identity, ngram_range=(1, 2), stop_words=stopwords)
vec_123 = TfidfVectorizer(preprocessor=identity, binary=True, min_df=10, tokenizer=identity, ngram_range=(1, 3), stop_words=stopwords)


vec_lr_1 = TfidfVectorizer(preprocessor=identity, norm="l1", min_df=10, binary=True, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords, max_features=4500)
vec_lr_12 = TfidfVectorizer(preprocessor=identity, tokenizer=identity, ngram_range=(1, 2), stop_words=stopwords, max_features=4500)
vec_lr_123 = TfidfVectorizer(preprocessor=identity, norm="l1", min_df=10, binary=True, tokenizer=identity, ngram_range=(1, 3), stop_words=stopwords, max_features=4500)

vec_lr2_1 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords, max_features=4500)
vec_lr2_12 = TfidfVectorizer(preprocessor=identity, tokenizer=identity, ngram_range=(1, 2), stop_words=stopwords, max_features=4500)
vec_lr2_123 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 3), stop_words=stopwords, max_features=4500)


vec_svm_1 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, min_df=10, ngram_range=(1, 1), stop_words=stopwords)
vec_svm_12 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, min_df=10, ngram_range=(1, 2), stop_words=stopwords)
vec_svm_123 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, min_df=10, ngram_range=(1, 3), stop_words=stopwords)

vec_svm2_1 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords)
vec_svm2_12 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 2), stop_words=stopwords)
vec_svm2_123 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 3), stop_words=stopwords)


party_list = {"cda": "cda", "christen-democratisch appèl": "cda", "cu": "cu", "christenunie": "cu",
              "d66": "d66", "democraten 66": "d66", "gl": "gl", "groenlinks": "gl", "pvda": "pvda", "partij van de arbeid": "pvda",
              "pvdd": "pvdd", "partij voor de dieren": "pvdd", "partijvddieren": "pvdd", "pvv": "pvv",
              "partij voor de vrijheid": "pvv", "sgp": "sgp", "staatukundig gereformeerde partij": "sgp",
              "sp": "sp", "socialistische partij": "sp", "vvd": "vvd", "volkspartij voor vrijheid en democratie": "vvd",
              "50plus": "50plus", "50+": "50plus"}


def write_parties_to_file(parties):
    """reads positive and negative files in, checks if tweet only has one unique party, either the abbreviation
    or full name, and if that's the case, writes it to the positive file for that party if it was a positive
    tweet, or to the negative file of that party if it was a negative one,
    note that I created the directories beforehand"""
    script_dir = path.dirname(__file__)
    rel_path = "classifier/testing/results/pos/positive_tweets.csv"
    abs_file_path = path.join(script_dir, rel_path)
    pos_tweets = []
    neg_tweets = []
    with open(abs_file_path, "r") as pos_file:
        reader = csv.reader(pos_file)
        for row in reader:
            pos_tweets.append(row)
    for tweet in pos_tweets:
        name = set()
        tweet_str = tweet[0]
        for party in parties.keys():
            if party in tweet_str:
                name.add(parties[party])
                break
        if len(name) == 1:
            item = list(name)[0]
            file = item + ".csv"
            with open("analysis/" + item + "/pos/" + file, "a") as pos_party:
                writer = csv.writer(pos_party, delimiter=" ")
                writer.writerow(tweet)

    rel_path = "classifier/testing/results/neg/negative_tweets.csv"
    abs_file_path = path.join(script_dir, rel_path)

    with open(abs_file_path, "r") as neg_file:
        reader = csv.reader(neg_file)
        for row in reader:
            neg_tweets.append(row)
    for tweet in neg_tweets:
        name = set()
        tweet_str = tweet[0]
        for party in parties.keys():
            if party in tweet_str:
                name.add(parties[party])
                break
        if len(name) == 1:
            item = list(name)[0]
            file = item + ".csv"
            with open("analysis/" + item + "/neg/" + file, "a") as neg_party:
                writer = csv.writer(neg_party, delimiter=" ")
                writer.writerow(tweet)


# best model used to classify remainder data:
# print("LINEAR SVM UNIGRAMS MODEL 2")
# classifier13 = Pipeline([("vec", vec_svm2_1), ("cls", svm.LinearSVC())])
# classifier13.fit(x_train, y_train)
# y_guess = classifier13.predict(x_test)


def write_test_to_file(X, y):
    """takes in list of test tweets and a list with guessed labels,
    sends tweets from list to file corresponding with label,
    e.g. if label was positive, it will be sent to the file 'positive_tweets.csv'
    used to write remainder data to positive and negative files, for later analysis of sentiment"""
    for index, item in enumerate(X):
        label = y[index]
        script_dir = path.dirname(__file__)
        if label == "pos":
            rel_path = "classifier/testing/results/pos/positive_tweets.csv"
            abs_file_path = path.join(script_dir, rel_path)
            with open(abs_file_path, "a", newline="") as f:
                writer = csv.writer(f, delimiter=" ")
                writer.writerow([item])
        elif label == "neg":
            rel_path = "classifier/testing/results/neg/negative_tweets.csv"
            abs_file_path = path.join(script_dir, rel_path)
            with open(abs_file_path, "a", newline="") as f:
                writer = csv.writer(f, delimiter=" ")
                writer.writerow([item])

# print(write_test_to_file(text_test, y_guess))

print("NAIVE BAYES UNIGRAMS")
classifier1 = Pipeline([("vec", vec_nb_1), ("cls", MultinomialNB(alpha=4.0))])
classifier1.fit(x_train, y_train)
y_guess = classifier1.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("NAIVE BAYES UNIGRAMS & BIGRAMS")
classifier2 = Pipeline([("vec", vec_nb_12), ("cls", MultinomialNB(alpha=5.0))])
classifier2.fit(x_train, y_train)
y_guess = classifier2.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("NAIVE BAYES UNIGRAMS, BIGRAMS & TRIGRAMS")
classifier3 = Pipeline([("vec", vec_nb_123), ("cls", MultinomialNB(alpha=6.0))])
classifier3.fit(x_train, y_train)
y_guess = classifier3.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("BERNOULLI NAIVE BAYES UNIGRAMS")
classifier4 = Pipeline([("vec", vec_1), ("cls", BernoulliNB(alpha=6.0))])
classifier4.fit(x_train, y_train)
y_guess = classifier4.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("BERNOULLI NAIVE BAYES UNIGRAMS & BIGRAMS")
classifier5 = Pipeline([("vec", vec_12), ("cls", BernoulliNB(alpha=6.0))])
classifier5.fit(x_train, y_train)
y_guess = classifier5.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("BERNOULLI NAIVE BAYES UNIGRAMS BIGRAMS & TRIGRAMS")
classifier6 = Pipeline([("vec", vec_123), ("cls", BernoulliNB(alpha=6.0))])
classifier6.fit(x_train, y_train)
y_guess = classifier6.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM SGDC UNIGRAMS")
classifier7 = Pipeline([("vec", vec_1), ("cls", linear_model.SGDClassifier())])
classifier7.fit(x_train, y_train)
y_guess = classifier7.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM SGDC UNIGRAMS & BIGRAMS")
classifier8 = Pipeline([("vec", vec_12), ("cls", linear_model.SGDClassifier())])
classifier8.fit(x_train, y_train)
y_guess = classifier8.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM SGDC UNIGRAMS BIGRAMS & TRIGRAMS")
classifier9 = Pipeline([("vec", vec_123), ("cls", linear_model.SGDClassifier())])
classifier9.fit(x_train, y_train)
y_guess = classifier9.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS MODEL 1")
classifier10 = Pipeline([("vec", vec_svm_1), ("cls", svm.LinearSVC())])
classifier10.fit(x_train, y_train)
y_guess = classifier10.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS BIGRAMS MODEL 1")
classifier11 = Pipeline([("vec", vec_svm_12), ("cls", svm.LinearSVC())])
classifier11.fit(x_train, y_train)
y_guess = classifier11.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS BIGRAMS TRIGRAMS MODEL 1")
classifier12 = Pipeline([("vec", vec_svm_123), ("cls", svm.LinearSVC())])
classifier12.fit(x_train, y_train)
y_guess = classifier12.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS MODEL 2")
classifier13 = Pipeline([("vec", vec_svm2_1), ("cls", svm.LinearSVC())])
classifier13.fit(x_train, y_train)
y_guess = classifier13.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS BIGRAMS MODEL 2")
classifier14 = Pipeline([("vec", vec_svm2_12), ("cls", svm.LinearSVC())])
classifier14.fit(x_train, y_train)
y_guess = classifier14.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS BIGRAMS TRIGRAMS MODEL 2")
classifier15 = Pipeline([("vec", vec_svm2_123), ("cls", svm.LinearSVC())])
classifier15.fit(x_train, y_train)
y_guess = classifier15.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM LINEAR KERNEL UNIGRAMS MODEL 1:")
classifier16 = Pipeline([("vec", vec_svm_1), ("cls", svm.SVC(kernel="linear"))])
classifier16.fit(x_train, y_train)
y_guess = classifier16.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM LINEAR KERNEL UNIGRAMS BIGRAMS MODEL 1:")
classifier17 = Pipeline([("vec", vec_svm_12), ("cls", svm.SVC(kernel="linear"))])
classifier17.fit(x_train, y_train)
y_guess = classifier17.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM LINEAR KERNEL UNIGRAMS BIGRAMS TRIGRAMS MODEL 1:")
classifier18 = Pipeline([("vec", vec_svm_123), ("cls", svm.SVC(kernel="linear"))])
classifier18.fit(x_train, y_train)
y_guess = classifier18.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))

print("SVM LINEAR KERNEL UNIGRAMS MODEL 2:")
classifier19 = Pipeline([("vec", vec_svm2_1), ("cls", svm.SVC(kernel="linear"))])
classifier19.fit(x_train, y_train)
y_guess = classifier19.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM LINEAR KERNEL UNIGRAMS BIGRAMS MODEL 2:")
classifier20 = Pipeline([("vec", vec_svm2_12), ("cls", svm.SVC(kernel="linear"))])
classifier20.fit(x_train, y_train)
y_guess = classifier20.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM LINEAR KERNEL UNIGRAMS BIGRAMS TRIGRAMS MODEL 2:")
classifier21 = Pipeline([("vec", vec_svm2_123), ("cls", svm.SVC(kernel="linear"))])
classifier21.fit(x_train, y_train)
y_guess = classifier21.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS MODEL 1")
classifier22 = Pipeline([("vec", vec_lr_1), ("cls", linear_model.LogisticRegression(C=1))])
classifier22.fit(x_train, y_train)
y_guess = classifier22.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS BIGRAMS MODEL 1")
classifier23 = Pipeline([("vec", vec_lr_12), ("cls", linear_model.LogisticRegression(C=1))])
classifier23.fit(x_train, y_train)
y_guess = classifier23.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS BIGRAMS TRIGRAMS MODEL 1")
classifier24 = Pipeline([("vec", vec_lr_123), ("cls", linear_model.LogisticRegression(C=1))])
classifier24.fit(x_train, y_train)
y_guess = classifier24.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS MODEL 2")
classifier25 = Pipeline([("vec", vec_lr2_1), ("cls", linear_model.LogisticRegression(C=1))])
classifier25.fit(x_train, y_train)
y_guess = classifier25.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS BIGRAMS MODEL 2")
classifier26 = Pipeline([("vec", vec_lr2_12), ("cls", linear_model.LogisticRegression(C=1))])
classifier26.fit(x_train, y_train)
y_guess = classifier26.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS BIGRAMS TRIGRAMS MODEL 2")
classifier27 = Pipeline([("vec", vec_lr2_123), ("cls", linear_model.LogisticRegression(C=1))])
classifier27.fit(x_train, y_train)
y_guess = classifier27.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


def show_most_informative_features(vectorizer, clf, n=20):
    feature_names = vectorizer.get_feature_names()
    coefs_with_features = sorted(zip(clf.coef_[0], feature_names))
    most_informative = zip(coefs_with_features[:n], coefs_with_features[:-(n + 1):-1])
    for (coef_1, fn_1), (coef_2, fn_2) in most_informative:
        print("\t%.4f\t%-15s\t\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2))

print("Most informative features Linear SVM Model 2 Unigrams")
print(show_most_informative_features(vec_svm2_1, classifier13.named_steps["cls"]))

print("Most informative features Linear SVM Model 2 Unigrams")
print(show_most_informative_features(vec_svm2_1, classifier13.named_steps["cls"], n=40))

print("Most informative features NB")
print(show_most_informative_features(vec_nb_12, classifier2.named_steps["cls"]))
print(show_most_informative_features(vec_nb_123, classifier3.named_steps["cls"]))

print("Most informative features Bernoulli NB")
print(show_most_informative_features(vec_12, classifier5.named_steps["cls"]))
print(show_most_informative_features(vec_123, classifier6.named_steps["cls"]))

print("Most informative features Linear SVM")
print(show_most_informative_features(vec_svm_12, classifier11.named_steps["cls"]))
print(show_most_informative_features(vec_svm_123, classifier12.named_steps["cls"]))

print(show_most_informative_features(vec_svm2_12, classifier14.named_steps["cls"]))
print(show_most_informative_features(vec_svm2_123, classifier15.named_steps["cls"]))

print("Most informative features Logistic Regression")
print(show_most_informative_features(vec_lr_12, classifier23.named_steps["cls"]))
print(show_most_informative_features(vec_lr_123, classifier24.named_steps["cls"]))

print(show_most_informative_features(vec_lr2_12, classifier26.named_steps["cls"]))
print(show_most_informative_features(vec_lr2_123, classifier27.named_steps["cls"]))
